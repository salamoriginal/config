args: {
    replicas: 1
}

profiles: {
    dev: {
        replicas: 1
    }
    test: {
        replicas: 2
    }
}

labels: {
    application: "votingapp"
}

containers: {
  
  voteui: {
    labels: {
      component: "voteui"
    }
    if args.dev {
      dirs: {
        "/usr/share/nginx/html": "../../vote-ui"
      }
    }
    build: {
      context: "../../vote-ui"
    }
    ports: publish : "80/http"
    scale: args.replicas
  }

  vote: {
    labels: {
      component: "vote"
    }
    build: {
      target: std.ifelse(args.dev, "dev", "production")
      context: "../../vote"
    }
    if args.dev {
      dirs: {
          "/app": "../../vote"
      }
    }
    ports: "5000/http"
  }

  redis: {
    labels: {
      component: "redis"
    }
    image: "redis:7.0.7-alpine3.17"
    ports: "6379/tcp"
    dirs: {
      if !args.dev {
        "/data": "volume://redis"
      }
    }
  }

  worker: {
    labels: {
      component: "worker"
    }
    build: "../../worker/go"
    env: {
     "POSTGRES_USER": "secret://db-creds/username"
     "POSTGRES_PASSWORD": "secret://db-creds/password"
    }
  }

  db: {
    labels: {
      component: "db"
    }
    image: "postgres:15.1-alpine3.17"
    ports: "5432/tcp"
    env: {
     "POSTGRES_USER": "secret://db-creds/username"
     "POSTGRES_PASSWORD": "secret://db-creds/password"
    }
    dirs: {
      if !args.dev {
        "/var/lib/postgresql/data": "volume://db"
      }
    }
  }

  result: {
    labels: {
      component: "result"
    }
    build: {
      target: std.ifelse(args.dev, "dev", "production")
      context: "../../result"
    }
    if args.dev {
      dirs: {
        "/app": "../../result"
      }
    }  
    ports: "5000/http"
    env: {
     "POSTGRES_USER": "secret://db-creds/username"
     "POSTGRES_PASSWORD": "secret://db-creds/password"
   }
  }

  resultui: {
    labels: {
      component: "resultui"
    }
    build: {
      target: std.ifelse(args.dev, "dev", "production")
      context: "../../result-ui"
    }
    if args.dev {
      dirs: {
        "/app/src": "../../result-ui/src"
      }
    } 
    ports: publish : "80/http"
  }
}

secrets: {
    "db-creds": {
        type: "basic"
        data: {
            username: ""
            password: ""
        }
    }
}

volumes: {
  if !args.dev {
    "db": {
        size: "100M"
    }
    "redis": {
        size: "100M"
    }
  }
}